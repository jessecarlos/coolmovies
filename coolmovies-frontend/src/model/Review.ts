export interface Review {
  id: string;
  title?: string;
  body?: string;
  rating?: number;
  reviewer?: Reviewer;
}

interface Reviewer {
  id: string;
  name: string;
}
