import { Review } from './Review';
import { User } from './User';

export interface Movie {
  id: string;
  title?: string;
  movieDirectorId?: string;
  userCreatorId?: string;
  releaseDate?: string;
  imgUrl?: string;
  director?: MovieDirector;
  userByUserCreatorId?: User;
  reviews?: Review[];
}

export interface MovieDirector {
  name: string;
}
