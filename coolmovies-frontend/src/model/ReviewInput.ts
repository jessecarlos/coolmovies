export interface ReviewInput {
  id?: string;
  title?: string;
  body?: string;
  rating?: number;
  movieId?: string;
  userReviewerId?: string;
}
