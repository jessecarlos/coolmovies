import React from 'react';
import { SvgIcon } from '@mui/material';
import EditSvgIcon from '../../public/edit.svg';
import Image from 'next/image';

export const EditIcon = (props) => {
  return (
    <SvgIcon
      {...props}
      component={() => <Image src={'/edit.svg'} width={20} height={20} />}
      inheritViewBox
    />
  );
};
