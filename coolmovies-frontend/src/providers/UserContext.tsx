import { createContext, useState } from 'react';
import { User } from '../model/User';

export const UserContext = createContext<{
  user?: User;
  updateUser?: Function;
}>({});

export const UserProvider = ({ children }) => {
  const [user, setUser] = useState<User | undefined>();
  return (
    <UserContext.Provider value={{ user, updateUser: setUser }}>
      {children}
    </UserContext.Provider>
  );
};
