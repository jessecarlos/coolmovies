import React from 'react';
import { styled } from '@mui/system';

export const AppContainer = styled('div')({
  backgroundColor: '#1C1C1C',
  display: 'flex',
  flexDirection: 'column',
  minHeight: '100vh',
  width: '100%',
  color: 'white',
});
