import React, { useCallback, useContext } from 'react';
import { makeStyles } from '@mui/styles';
import {
  AppBar,
  Box,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
} from '@mui/material';
import { AccountCircle } from '@mui/icons-material';
import { UserContext } from '../../providers/UserContext';
import { User } from '../../model/User';
import { useFetchUsers } from '../../hooks/useFetchUsers';

const useStyles = makeStyles({
  paper: {
    background: '#000',
    color: 'white',
    textAlign: 'end',
  },
});

export const AppHeader = () => {
  const classes = useStyles();
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null
  );

  const handleOpenUserMenu = useCallback(
    (event: React.MouseEvent<HTMLElement>) => {
      setAnchorElUser(event.currentTarget);
    },
    []
  );

  const handleCloseUserMenu = useCallback(() => {
    setAnchorElUser(null);
  }, []);

  const { user, updateUser } = useContext(UserContext);
  const handleUpdateUser = useCallback(
    (user: User) => () => {
      if (updateUser) {
        updateUser(user);
      }
      handleCloseUserMenu();
    },
    [handleCloseUserMenu, updateUser]
  );

  const { data } = useFetchUsers();

  return (
    <AppBar position={'static'} className={classes.paper}>
      <Toolbar>
        <Box sx={{ flexGrow: 1 }}>
          {user?.name}
          <IconButton onClick={handleOpenUserMenu} color="inherit">
            <AccountCircle />
          </IconButton>
        </Box>
        <Menu
          id="menu-appbar"
          anchorEl={anchorElUser}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          keepMounted
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={Boolean(anchorElUser)}
          onClose={handleCloseUserMenu}
        >
          {data?.map((user) => (
            <MenuItem key={user.id} onClick={handleUpdateUser(user)}>
              {user.name}
            </MenuItem>
          ))}
        </Menu>
      </Toolbar>
    </AppBar>
  );
};
