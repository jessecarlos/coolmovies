import React from 'react';
import { styled } from '@mui/system';

export const AppFooter = styled('div')({
  border: '1px solid black',
  display: 'flex',
  flexDirection: 'column',
  alignSelf: 'self-end',
  width: '100%',
});
