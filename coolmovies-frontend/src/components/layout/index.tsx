import React from 'react';
import { AppContainer } from './AppContainer';
import { AppContent } from './AppContent';
import { AppFooter } from './AppFooter';
import { AppHeader } from './AppHeader';

const Layout = ({ children }) => {
  return (
    <AppContainer>
      <AppHeader />
      <AppContent>{children}</AppContent>
      <AppFooter>Footer</AppFooter>
    </AppContainer>
  );
};

export { Layout };
