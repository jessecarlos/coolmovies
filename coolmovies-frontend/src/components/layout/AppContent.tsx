import React from 'react';
import { styled } from '@mui/system';

export const AppContent = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
  flexGrow: 1,
});
