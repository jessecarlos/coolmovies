import { Box, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { useRouter } from 'next/router';
import { Movie } from '../../model/Movie';
import { useCallback } from 'react';

interface MovieCardProps {
  movie?: Movie;
}

const useStyles = makeStyles({
  card: {
    display: 'flex',
    flexDirection: 'column',
    width: 300,
    height: 450,
    justifySelf: 'center',
    borderRadius: '2px',
    cursor: 'pointer',
    '&:hover': { opacity: '0.75' },
  },
  image: {
    height: '350px',
    width: '100%',
    borderRadius: '2px 2px 0 0',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    color: 'white',
    backgroundColor: '#222222',
    padding: 6,
    height: '100%',
  },
});

const MovieCard: React.FC<MovieCardProps> = ({ movie }) => {
  const classes = useStyles();
  const router = useRouter();
  const handleClick = useCallback(
    () => router.push(`/reviews/${movie?.id}`),
    [router, movie?.id]
  );
  return (
    <Box boxShadow={2} className={classes.card} onClick={handleClick}>
      <img className={classes.image} src={movie?.imgUrl} />
      <div className={classes.content}>
        <div>
          <Typography gutterBottom variant="h6" component="div">
            {movie?.title}
          </Typography>
        </div>
        <div>
          <Typography gutterBottom variant={'subtitle1'} component="div">
            2011
          </Typography>
        </div>
      </div>
    </Box>
  );
};

export { MovieCard };
