export { actions as usersActions } from './usersSlice';
export { default as usersReducer } from './usersSlice';
import { combineEpics } from 'redux-observable';
import { usersAsyncEpic } from './usersEpics';

export const usersEpics = combineEpics(usersAsyncEpic);
