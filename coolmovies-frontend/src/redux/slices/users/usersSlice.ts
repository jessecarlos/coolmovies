import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { User } from '../../../model/User';

interface UsersState {
  data?: User[];
  loading: boolean;
  error?: string;
}

const initialState: UsersState = {
  loading: false,
};

export const usersSlice = createSlice({
  initialState,
  name: 'users',
  reducers: {
    fetch: (state) => {
      state.loading = true;
      state.error = undefined;
    },
    loaded: (state, action: PayloadAction<{ data: User[] }>) => {
      state.loading = false;
      state.data = action.payload.data;
      state.error = undefined;
    },
    loadError: (state) => {
      state.loading = false;
      state.error = 'Error fetching users';
    },
  },
});

export const { actions } = usersSlice;
export type SliceAction = typeof actions;
export default usersSlice.reducer;
