import { Epic, StateObservable } from 'redux-observable';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { RootState } from '../../store';
import { EpicDependencies } from '../../types';
import { actions, SliceAction } from './usersSlice';
import { allUsersQuery } from '../../../graphql/queries/allUsersQuery';

export const usersAsyncEpic: Epic = (
  action$: Observable<SliceAction['fetch']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetch.match),
    switchMap(async () => {
      try {
        const result = await client.query({
          query: allUsersQuery,
        });
        return actions.loaded({ data: result.data.allUsers.nodes });
      } catch (err) {
        return actions.loadError();
      }
    })
  );
