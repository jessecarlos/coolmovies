import type { NextPage } from 'next';
import { MovieCard } from '../components/movieCard';
import { makeStyles } from '@mui/styles';
import { useFetchMovies } from '../hooks/useFetchMovies';

const useStyles = makeStyles({
  root: {
    width: '100%',
    display: 'grid',
    gridTemplateColumns: 'repeat(3, 300px)',
    rowGap: 30,
    columnGap: 30,
    padding: 16,
    marginTop: 32,
  },
});

const Home: NextPage = () => {
  const classes = useStyles();
  const { data } = useFetchMovies();

  return (
    <div className={classes.root}>
      {data?.map((movie) => (
        <MovieCard key={movie.id} movie={movie} />
      ))}
    </div>
  );
};

export default Home;
