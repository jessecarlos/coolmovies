import React, { useCallback, useContext } from 'react';
import { makeStyles } from '@mui/styles';
import { Button } from '@mui/material';
import { useDispatch } from 'react-redux';
import { moviesActions, useAppSelector } from '../../redux';
import { UserContext } from '../../providers/UserContext';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '400px',
    borderRadius: '10px',
    rowGap: '5px',
  },
  image: {
    height: '400px',
    borderRadius: '5px',
  },
});

const MovieImage: React.FC<{ imgUrl?: string }> = ({ imgUrl }) => {
  const classes = useStyles();
  const editingReview = useAppSelector((state) => state.movies.editingReview);
  const { user } = useContext(UserContext);
  const dispatch = useDispatch();
  const handleAddReview = useCallback(() => {
    dispatch(
      moviesActions.editingMovieReview({
        id: '',
        title: '',
        body: '',
        rating: 0,
      })
    );
  }, [dispatch]);

  return (
    <div className={classes.root}>
      {imgUrl && (
        <img src={imgUrl} className={classes.image} alt={'Movie image'} />
      )}
      <Button
        variant={'outlined'}
        disabled={!!editingReview || !user}
        onClick={handleAddReview}
      >
        Add Review
      </Button>
    </div>
  );
};

export { MovieImage };
export default MovieImage;
