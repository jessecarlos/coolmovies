import React from 'react';
import { useRouter } from 'next/router';
import { useFetchMovieById } from '../../hooks/useFetchMovieById';
import { makeStyles } from '@mui/styles';
import { Grid, Typography } from '@mui/material';
import { Movie } from '../../model/Movie';
import { MovieTitle } from './MovieTitle';
import { MovieImage } from './MovieImage';
import { ReviewsList } from './ReviewsList';
import { ReviewForm } from './ReviewForm';
import { useAppSelector } from '../../redux';

const useStyles = makeStyles({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    padding: 24,
  },
  movieSection: {
    width: '100%',
    display: 'flex',
    flexGrow: 1,
    flexWrap: 'wrap',
    justifyContent: 'center',
    columnGap: 80,
  },
  image: {
    borderRadius: '5px',
  },
  reviewSection: {
    flexGrow: 2,
    flexBasis: '60%',
  },
});

const Reviews = () => {
  const classes = useStyles();
  const router = useRouter();
  const { movieId } = router.query;
  const { loading, movie } = useFetchMovieById(movieId);
  const editingReview = useAppSelector((state) => state.movies.editingReview);

  return (
    <div className={classes.root}>
      <MovieTitle movie={movie} />
      <div className={classes.movieSection}>
        <MovieImage imgUrl={movie?.imgUrl} />
        <div className={classes.reviewSection}>
          {editingReview && <ReviewForm movieId={movieId} />}
          <ReviewsList reviews={movie?.reviews} />
        </div>
      </div>
    </div>
  );
};

export default Reviews;
