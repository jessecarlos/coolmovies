import React from 'react';
import { useRouter } from 'next/router';
import { useFetchMovieById } from '../../hooks/useFetchMovieById';
import { makeStyles } from '@mui/styles';
import { Grid, Typography } from '@mui/material';
import { Movie } from '../../model/Movie';

const useStyles = makeStyles({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '16px',
  },
});

const MovieTitle: React.FC<{ movie?: Movie }> = ({ movie }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography variant={'h4'}>{movie?.title}</Typography>
      <Typography>Director: {movie?.director?.name}</Typography>
    </div>
  );
};

export { MovieTitle };
export default MovieTitle;
