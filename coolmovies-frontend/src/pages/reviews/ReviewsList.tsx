import React from 'react';
import { makeStyles } from '@mui/styles';
import { Review } from '../../model/Review';
import { ReviewCard } from './ReviewsCard';

const useStyles = makeStyles({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    rowGap: '16px',
  },
  addReview: {
    width: '100%',
    textAlign: 'end',
  },
});

const ReviewsList: React.FC<{ reviews?: Review[] }> = ({ reviews }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {reviews?.map((review) => (
        <ReviewCard key={review.id} review={review} />
      ))}
    </div>
  );
};

export { ReviewsList };
export default ReviewsList;
