import React, { useCallback, useContext, useEffect, useState } from 'react';
import {
  Button,
  Card,
  CardContent,
  Grid,
  Rating,
  TextField,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import { moviesActions, useAppDispatch, useAppSelector } from '../../redux';
import { actions } from '../../redux/slices/movies/moviesSlice';
import { UserContext } from '../../providers/UserContext';

const useStyles = makeStyles({
  card: {
    marginBottom: '20px',
  },
  actions: {
    display: 'flex',
    columnGap: '5px',
    justifyContent: 'end',
  },
});

const ReviewForm = ({ movieId }) => {
  const classes = useStyles();
  const [title, setTitle] = useState<string | undefined>('');
  const [body, setBody] = useState<string | undefined>('');
  const [rating, setRating] = useState<number | undefined>(0);
  const [errors, setErrors] = useState<any | undefined>();
  const handleChange = useCallback(
    (fieldName, updateField) => (e) => updateField(e.currentTarget?.value),
    []
  );
  const handleRating = useCallback(
    (updateField) => (event, newValue) => updateField(newValue),
    []
  );
  const dispatch = useAppDispatch();
  const handleCancelReview = useCallback(() => {
    dispatch(moviesActions.cancelMovieReview());
  }, [dispatch]);
  const { user } = useContext(UserContext);

  const reviewToUpdate = useAppSelector((state) => state.movies?.editingReview);
  useEffect(() => {
    setTitle(reviewToUpdate?.title);
    setBody(reviewToUpdate?.body);
    setRating(reviewToUpdate?.rating);
  }, [reviewToUpdate]);

  const onSubmit = useCallback(
    (title, body, rating) => {
      return () => {
        let errors;

        if (!title) {
          errors = { ...(errors || {}), title: 'Required' };
        }
        if (!body) {
          errors = { ...(errors || {}), body: 'Required' };
        }
        setErrors(errors);

        if (errors) {
          return;
        }

        if (reviewToUpdate?.id) {
          const { id } = reviewToUpdate;
          dispatch(
            actions.updateMovieReviewEpic({
              id,
              movieReviewPatch: {
                title,
                body,
                rating: rating || 0,
                userReviewerId: user?.id,
                movieId,
              },
            })
          );
          return;
        }
        dispatch(
          actions.createMovieReviewEpic({
            title,
            body,
            rating: rating || 0,
            userReviewerId: user?.id,
            movieId,
          })
        );
      };
    },
    [dispatch, user?.id, movieId, reviewToUpdate]
  );

  return (
    <Card className={classes.card}>
      <CardContent>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              id="title"
              name="title"
              label="Title"
              fullWidth
              onChange={handleChange('title', setTitle)}
              helperText={errors?.title || ''}
              error={!!errors?.title}
              value={title}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="body"
              name="body"
              label="Review"
              multiline
              fullWidth
              minRows={7}
              onChange={handleChange('body', setBody)}
              helperText={errors?.body || ''}
              error={!!errors?.body}
              value={body}
            />
          </Grid>
          <Grid item xs={12}>
            <Rating
              id="rating"
              name="rating"
              value={rating}
              onChange={handleRating(setRating)}
            />
          </Grid>
          <Grid item xs={12}>
            <div className={classes.actions}>
              <Button
                variant={'text'}
                color={'error'}
                onClick={handleCancelReview}
              >
                Cancel
              </Button>
              <Button
                variant={'outlined'}
                onClick={onSubmit(title, body, rating)}
              >
                Save
              </Button>
            </div>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export { ReviewForm };
export default ReviewForm;
