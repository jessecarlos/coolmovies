import React, { useCallback, useContext } from 'react';
import { makeStyles } from '@mui/styles';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Rating,
  Stack,
} from '@mui/material';
import { Review } from '../../model/Review';
import { Delete } from '@mui/icons-material';
import { moviesActions, useAppDispatch } from '../../redux';
import { UserContext } from '../../providers/UserContext';
import { EditIcon } from '../../icons/EditIcon';

const SUB_HEADER_COLOR = '#61DAFC';

const useStyles = makeStyles({
  root: {
    backgroundColor: '#242424',
    color: 'white',
  },
  // @ts-ignore
  actions: ({ shouldShowActions }) => ({
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: '10px',
    flexDirection: shouldShowActions ? 'row' : 'row-reverse',
  }),
});

const ReviewCard: React.FC<{ review: Review }> = ({ review }) => {
  const dispatch = useAppDispatch();
  const handleEditReview = useCallback(() => {
    dispatch(moviesActions.editingMovieReview(review));
  }, [dispatch, review]);
  const handleDeleteReview = useCallback(() => {
    dispatch(moviesActions.deleteMovieReviewEpic({ id: review.id }));
  }, [dispatch, review.id]);
  const { user } = useContext(UserContext);
  const shouldShowActions = user && user.id === review.reviewer?.id;
  const classes = useStyles({ shouldShowActions });

  return (
    <Card className={classes.root}>
      <CardHeader
        title={review.title}
        titleTypographyProps={{ variant: 'h6' }}
        subheader={review.reviewer?.name}
        subheaderTypographyProps={{
          variant: 'subtitle2',
          color: SUB_HEADER_COLOR,
        }}
      />
      <CardContent>
        <div>{review.body}</div>
        <div className={classes.actions}>
          {shouldShowActions && (
            <Stack direction="row" spacing={2}>
              <Button
                endIcon={<EditIcon color={'primary'} />}
                variant={'outlined'}
                onClick={handleEditReview}
              >
                Edit
              </Button>
              <Button
                endIcon={<Delete />}
                color={'error'}
                variant={'outlined'}
                onClick={handleDeleteReview}
              >
                Remove
              </Button>
            </Stack>
          )}
          <Rating value={review.rating || 0} readOnly />
        </div>
      </CardContent>
    </Card>
  );
};

export { ReviewCard };
export default ReviewCard;
