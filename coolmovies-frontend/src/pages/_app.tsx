import '../styles/globals.css';
import type { AppProps } from 'next/app';
import React, { FC, useMemo, useState } from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import Head from 'next/head';
import { createStore } from '../redux';
import { EnhancedStore } from '@reduxjs/toolkit';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import { Layout } from '../components/layout';
import { CssBaseline } from '@mui/material';
import { UserProvider } from '../providers/UserContext';

const App: FC<AppProps> = ({ Component, pageProps }) => {
  const [store, setStore] = useState<EnhancedStore | null>(null);
  const apolloClient = useMemo(() => {
    const client = new ApolloClient({
      cache: new InMemoryCache({ addTypename: false }),
      uri: process.env.NEXT_PUBLIC_GRAPHQL_URL,
    });

    const store = createStore({ epicDependencies: { client } });
    setStore(store);

    return client;
  }, []);

  if (!store) return <>{'Loading...'}</>;

  return (
    <>
      <Head>
        <title>{'Coolmovies Frontend'}</title>
        <meta charSet="UTF-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <UserProvider>
        <ReduxProvider store={store}>
          <ApolloProvider client={apolloClient}>
            <CssBaseline />
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </ApolloProvider>
        </ReduxProvider>
      </UserProvider>
    </>
  );
};

export default App;
