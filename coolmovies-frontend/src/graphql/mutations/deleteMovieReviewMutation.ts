import { gql } from '@apollo/client';

export const deleteMovieReviewMutation = gql`
  mutation Mutation($id: DeleteMovieReviewByIdInput!) {
    deleteMovieReviewById(input: $id) {
      movie: movieByMovieId {
        title
        releaseDate
        imgUrl
        director: movieDirectorByMovieDirectorId {
          name
        }
        reviews: movieReviewsByMovieId {
          nodes {
            id
            title
            body
            rating
            reviewer: userByUserReviewerId {
              id
              name
            }
          }
        }
      }
    }
  }
`;
