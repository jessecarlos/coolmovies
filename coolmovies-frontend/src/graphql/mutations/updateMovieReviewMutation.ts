import { gql } from '@apollo/client';

export const updateMovieReviewMutation = gql`
  mutation Mutation($movieReviewInput: UpdateMovieReviewByIdInput!) {
    updateMovieReviewById(input: $movieReviewInput) {
      movie: movieByMovieId {
        title
        releaseDate
        imgUrl
        director: movieDirectorByMovieDirectorId {
          name
        }
        reviews: movieReviewsByMovieId {
          nodes {
            id
            title
            body
            rating
            reviewer: userByUserReviewerId {
              id
              name
            }
          }
        }
      }
    }
  }
`;
