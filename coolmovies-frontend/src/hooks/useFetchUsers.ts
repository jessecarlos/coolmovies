import { useAppDispatch, useAppSelector, usersActions } from '../redux';
import { useEffect } from 'react';

export const useFetchUsers = () => {
  const dispatch = useAppDispatch();
  const usersState = useAppSelector((state) => state.users);
  useEffect(() => {
    dispatch(usersActions.fetch());
  }, [dispatch]);

  return { loading: usersState.loading, data: usersState.data };
};
