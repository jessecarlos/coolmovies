import { useEffect } from 'react';
import { Movie } from '../model/Movie';
import { moviesActions, useAppDispatch, useAppSelector } from '../redux';

interface FetchMovieResult {
  loading: boolean;
  movie: Movie;
}

export const useFetchMovieById = (movieId?: any): FetchMovieResult => {
  const dispatch = useAppDispatch();
  const { loading, movie, reviews } = useAppSelector(
    (state) => state.movies.reviews
  );

  useEffect(() => {
    dispatch(moviesActions.fetchReviewsByMovieId({ movieId }));
  }, [dispatch, movieId]);

  return {
    loading,
    movie: { id: '', ...movie, reviews: reviews },
  };
};
