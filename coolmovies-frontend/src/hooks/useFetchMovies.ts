import { moviesActions, useAppDispatch, useAppSelector } from '../redux';
import { useEffect } from 'react';

export const useFetchMovies = () => {
  const dispatch = useAppDispatch();
  const moviesState = useAppSelector((state) => state.movies);
  useEffect(() => {
    dispatch(moviesActions.fetch());
  }, [dispatch]);

  return { loading: moviesState.loading, data: moviesState.data };
};
